package suitebot3.ai;

import suitebot3.game.GameSetup;
import suitebot3.game.GameState;
import suitebot3.game.Moves;

public class SampleBotAi implements BotAi
{
	public SampleBotAi(GameSetup gameSetup)
	{
	}

	@Override
	public Moves makeMoves(GameState gameState)
	{
		return new Moves();
	}
}
