package suitebot3.game;

import suitebot3.game.dto.GameStateDTO;

import java.util.List;

public class GameSetup
{
	public final int aiPlayerId;
	public final List<Player> players;
	public final GamePlan gamePlan;

	public GameSetup(int aiPlayerId, List<Player> players, GamePlan gamePlan)
	{
		this.aiPlayerId = aiPlayerId;
		this.players = players;
		this.gamePlan = gamePlan;
	}

	public static GameSetup fromGameStateDto(GameStateDTO dto)
	{
		return new GameSetup(dto.aiPlayerId, null, null);
	}
}
