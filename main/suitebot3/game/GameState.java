package suitebot3.game;

import suitebot3.game.dto.GameStateDTO;

public interface GameState
{
    int getCurrentRound();
    int getRoundsRemaining();

    GameStateDTO toDto();
}
