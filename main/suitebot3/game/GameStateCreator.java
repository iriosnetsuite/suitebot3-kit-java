package suitebot3.game;

import suitebot3.game.dto.GameStateDTO;

public class GameStateCreator
{
	public static StandardGameState fromString(String gameStateAsString)
	{
		return new StandardGameState(1, 10);
	}

	public static StandardGameState fromDto(GameStateDTO dto)
	{
		return new StandardGameState(dto.currentRound, dto.remainingRounds);
	}
}
