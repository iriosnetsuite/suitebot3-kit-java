package suitebot3.game.dto;

import com.google.gson.Gson;

/**
 * Do not modify.
 */
public class JsonUtil
{
	public static GameStateDTO decodeGameState(String json)
	{
		return new Gson().fromJson(json, GameStateDTO.class);
	}
}
