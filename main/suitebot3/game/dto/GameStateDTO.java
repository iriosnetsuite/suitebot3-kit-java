package suitebot3.game.dto;

/**
 * Do not modify.
 */
public class GameStateDTO
{
    public int aiPlayerId;

    public int currentRound;
    public int remainingRounds;
}